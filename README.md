--------------------------------------------------------------------------
Implémentation du wakeword pour agent conversationnel
=============

Infos générales
---------------

- **Stagiaire** : Théo Moulin - theo.moulin@edu.hefr.ch
- **Superviseur** : [Mira El Kamali](https://gitlab.forge.hefr.ch/mira.elkamali) - mira.elkamali@hefr.ch
- **Professeur** : [Elena Mugellini](https://gitlab.forge.hefr.ch/elena.mugellini) - elena.mugellini@hefr.ch
- **Professeur** : [Omar Abou Khaled](https://gitlab.forge.hefr.ch/omar.aboukhaled) - omar.aboukhaled@hefr.ch
- **Dates** : du 08.06.2021 au 03.09.2021


Contexte
--------

Ce projet a été développé durant un stage effectué à l'institut HumanTech de la Haute école d'ingénierie et d'architecture de Fribourg

Description
-----------

Le but de ce projet est d'implémenter un nouveau système de wakeword pour l'agent conversationnel Nestore afin d'améliorer ses performances.

Installation 
-----------

Raven est l'algorithme ayant été retenu, pour l'installer : 

git clone https://github.com/rhasspy/rhasspy-wake-raven
cd rhasspy-wake-raven
./configure
Make
Make install

Test
----
Pour record les samples : 

arecord -r 16000 -f S16_LE -c 1 -t raw | bin/rhasspy-wake-raven --record keyword-dir/”test”

Pour tester les samples : 

arecord -r 16000 -f S16_LE -c 1 -t raw | bin/rhasspy-wake-raven --keyword keyword-dir/”test”

pour tester depuis un script -> /code/rhasspy-wake-raven/wake.py

Contenu
-------

Ce dépôt contient toute la documentation relative au projet dans le dossier `docs/`. Le code du projet est dans le dossier `code/`.
