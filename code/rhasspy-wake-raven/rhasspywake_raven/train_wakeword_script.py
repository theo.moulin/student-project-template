import __main__
import argparse

from pathlib import Path
from rhasspysilence import WebRtcVadRecorder
from rhasspysilence.const import SilenceMethod

# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(prog="rhasspy-wake-raven")

def record_templates(
    record_dir: Path,
    name_format: str,
    recorder: WebRtcVadRecorder,
    args: argparse.Namespace,
):
    """Record audio templates."""
    print("Reading 16-bit 16Khz mono audio from stdin...", file=sys.stderr)

    num_templates = 0

    try:
        print(
            f"Recording template {num_templates}. Please speak your wake word. Press CTRL+C to exit."
        )
        recorder.start()

        while num_templates<2 : #while i=o; i<4, i++ 
            # Read raw audio chunk
            chunk = sys.stdin.buffer.read(recorder.chunk_size)
            if not chunk:
                # Empty chunk
                break

            result = recorder.process_chunk(chunk)
            if result:
                audio_bytes = recorder.stop()
                audio_bytes = trim_silence(audio_bytes)

                template_path = record_dir / name_format.format(n=num_templates)
                template_path.parent.mkdir(parents=True, exist_ok=True)

                wav_bytes = buffer_to_wav(audio_bytes)
                template_path.write_bytes(wav_bytes)
                _LOGGER.debug(
                    "Wrote %s byte(s) of WAV audio to %s", len(wav_bytes), template_path
                )

                num_templates += 1
                print(
                    f"Recording template {num_templates}. Please speak your wake word. Press CTRL+C to exit."
                )
                recorder.start()
    except KeyboardInterrupt:
        print("Done")
        
        
        
        
args = parser.parse_args()


record_dir = Path(args.record[0])
if len(args.record) > 1:
    record_format = args.record[1]
else:
    record_format = "example-{n:02d}.wav"

record_templates(record_dir, record_format, recorder, args)




