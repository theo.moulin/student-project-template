import subprocess
import os

#cd rhasppy-wake-raven

def wakeword(param) :

    if param=="0" :
        print("You will need to speak your wakeword 3 times")
        commande="arecord -r 16000 -f S16_LE -c 1 -t raw |  bin/rhasspy-wake-raven --record keyword-dir/'nestor_test'"
        os.system(commande)

    elif param=="1" :
        commande="arecord -r 16000 -f S16_LE -c 1 -t raw |  bin/rhasspy-wake-raven --keyword keyword-dir/'nestor_test'"
        os.system(commande)


print ("What do you want to do ? 0:record 1:listen")
param=input()

wakeword(param)